require "json"
require_relative "period"

class ShiftManager
  attr_accessor :periods

  def initialize(periods)
    @periods = periods
  end

  # Parse JSON file with periods data and build a ShiftManager instance with its
  # array of Period objects
  def self.new_from_json_file(filename='data.json')
    data = File.read(filename)
    data_hash = JSON.parse(data)
    periods_data_array = data_hash['periods']
    periods = periods_data_array.map do |period_data|
      Period.new_from_hash(period_data)
    end
    ShiftManager.new(periods)
  end

  def to_json
    { 'availabilities': availabilities }
  end

  def write_availabilities_to_json_file(output_file='output.json')
    json_output = JSON.pretty_generate(self.to_json)
    File.write(output_file, json_output)
  end

  private

  # For each Period counts days by type.
  # Returns an array of hashes, each one representing the output of calculation
  # for each Period
  def availabilities
    periods.map do |period|
      period.count_days_by_type
      period.to_json
    end
  end
end

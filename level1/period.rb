require "date"
require "holidays"
require "holidays/core_extensions/date"

class Date
  include Holidays::CoreExtensions::Date
end

class Period
  attr_accessor :id, :start_date, :end_date
  attr_reader :total_days, :workdays, :weekend_days, :holidays

  def initialize(id, start_date, end_date)
    @id = id
    @start_date = start_date
    @end_date = end_date
    @total_days = (end_date - start_date + 1).to_i
    @workdays = 0
    @weekend_days = 0
    @holidays = 0
  end

  def self.new_from_hash(period_data)
    id = period_data['id']
    start_date = parse_date(period_data['since'])
    end_date = parse_date(period_data['until'])
    Period.new(id, start_date, end_date)
  end

  def count_days_by_type
    start_date.upto(end_date) do |date|
      if weekend_day(date)
        @weekend_days += 1
      elsif holiday(date)
        @holidays += 1
      else
        @workdays += 1
      end
    end
  end

  def to_json
    {
      'period_id': id,
      'total_days': total_days,
      'workdays': workdays,
      'weekend_days': weekend_days,
      'holidays': holidays
    }
  end

  private

  def weekend_day(date)
    date.wday == 0 || date.wday == 6
  end

  def holiday(date, locale=:it)
    date.holiday?(locale)
  end

  def self.parse_date(string_date, format='%Y-%m-%d')
    Date.strptime(string_date, format)
  end

end

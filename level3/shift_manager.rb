require "json"
require_relative "developer"
require_relative "project"

class ShiftManager
  attr_accessor :projects, :developers, :local_holidays

  def initialize(developers, projects, local_holidays)
    @developers = developers
    @projects = projects
    @local_holidays = local_holidays
  end

  # Parse JSON file with data and build a ShiftManager instance with its
  # array of associated objects
  def self.new_from_json_file(filename='data.json')
    data = File.read(filename)
    data_hash = JSON.parse(data)

    developers = initialize_objects_from_data_hash('developers', Developer, data_hash)
    projects = initialize_objects_from_data_hash('projects', Project, data_hash)
    local_holidays = initialize_objects_from_data_hash('local_holidays', LocalHoliday, data_hash)

    ShiftManager.new(developers, projects, local_holidays)
  end

  def to_json
    { 'availabilities': availabilities }
  end

  def write_availabilities_to_json_file(output_file='output.json')
    json_output = JSON.pretty_generate(self.to_json)
    # json_output = self.to_json
    File.write(output_file, json_output)
  end

  private

  # Returns an array of objects of type <klass> initialized with <field> data
  # from <data_hash>
  def self.initialize_objects_from_data_hash(field, klass, data_hash)
    objects_data_array = data_hash[field]
    objects = objects_data_array.map do |object_data|
      klass.new_from_hash(object_data)
    end
  end

  # For each Project, counts days by type in the given Period.
  # Returns an array of hashes, each one representing the output of calculation
  # for each Project, including his feasibility in the given Period.
  def availabilities
    projects.map do |project|
      project.period.count_days_by_type
      project.period.to_json.merge({ 'feasibility': project_feasible?(project) })
    end
  end

  def project_feasible?(project)
    # The number of total workdays available is the sum of developers' workdays
    # in the period planned for the project
    total_workdays = developers.map do |developer|
      project.period.count_days_by_type(developer)
      project.period.workdays
    end.reduce(:+)
    total_workdays > project.effort_days
  end
end

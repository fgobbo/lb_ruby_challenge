require_relative 'period'

class Developer
  attr_accessor :id, :name, :birthday

  def initialize(id, name, birthday)
    @id = id
    @name = name
    @birthday = birthday
  end

  def self.new_from_hash(developer_data)
    id = developer_data['id']
    name = developer_data['name']
    birthday = Period.parse_date(developer_data['birthday'])
    Developer.new(id, name, birthday)
  end
end

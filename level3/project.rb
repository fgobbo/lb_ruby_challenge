class Project
  attr_accessor :id, :period, :effort_days

  def initialize(id, period, effort_days)
    @id = id
    @period = period
    @effort_days = effort_days
  end

  def self.new_from_hash(project_data)
    id = project_data['id']
    start_date = Period.parse_date(project_data['since'])
    end_date = Period.parse_date(project_data['until'])
    period = Period.new(id, start_date, end_date)
    effort_days = project_data['effort_days']
    Project.new(id, period, effort_days)
  end
end

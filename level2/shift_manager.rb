require "json"
require_relative "developer"
require_relative "period"

class ShiftManager
  attr_accessor :periods, :developers, :local_holidays

  def initialize(developers, periods, local_holidays)
    @developers = developers
    @periods = periods
    @local_holidays = local_holidays
  end

  # Parse JSON file with data and build a ShiftManager instance with its
  # array of associated objects
  def self.new_from_json_file(filename='data.json')
    data = File.read(filename)
    data_hash = JSON.parse(data)

    developers = initialize_objects_from_data_hash('developers', Developer, data_hash)
    periods = initialize_objects_from_data_hash('periods', Period, data_hash)
    local_holidays = initialize_objects_from_data_hash('local_holidays', LocalHoliday, data_hash)

    ShiftManager.new(developers, periods, local_holidays)
  end

  def to_json
    { 'availabilities': availabilities }
  end

  def write_availabilities_to_json_file(output_file='output.json')
    json_output = JSON.pretty_generate(self.to_json)
    # json_output = self.to_json
    File.write(output_file, json_output)
  end

  private

  # Returns an array of objects of type <klass> initialized with <field> data
  # from <data_hash>
  def self.initialize_objects_from_data_hash(field, klass, data_hash)
    objects_data_array = data_hash[field]
    objects = objects_data_array.map do |object_data|
      klass.new_from_hash(object_data)
    end
  end

  # For each Period counts days by type per Developer.
  # Returns an array of hashes, each one representing the output of calculation
  # for each Period per each Developer
  def availabilities
    periods.map do |period|
      developers.map do |developer|
        period.count_days_by_type(developer)
        { 'developer_id': developer.id }.merge(period.to_json)
      end
    end.flatten
  end
end

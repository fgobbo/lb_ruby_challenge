require 'date'

class LocalHoliday
  attr_accessor :day, :name

  @@all_dates = []

  def initialize(day, name)
    @day = day
    @name = name
    @@all_dates << @day if @day.is_a? Date
  end

  def self.new_from_hash(holiday_data)
    day = Period.parse_date(holiday_data['day'])
    name = holiday_data['name']
    LocalHoliday.new(day, name)
  end

  def self.all_dates
    @@all_dates
  end

end
